package kz.aitu.midterm.controller;


import kz.aitu.midterm.model.Person;
import kz.aitu.midterm.service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonController {
    private PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }
    @GetMapping("/api/v2/users/")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(personService.getAll());
    }

    @PostMapping("/api/v2/users/")
    public ResponseEntity<?> createUser(@RequestBody Person person){
        return ResponseEntity.ok(personService.createPerson(person));
    }

    @PutMapping("/api/v2/users/")
    public ResponseEntity<?> updateUser(@RequestBody Person person){
        return ResponseEntity.ok(personService.createPerson(person));
    }

    @DeleteMapping("/api/v2/users/{id}")
    public void deleteUser(@PathVariable long id){
        personService.deleteById(id);
    }
}
