package kz.aitu.midterm.service;


import kz.aitu.midterm.model.Person;
import kz.aitu.midterm.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {
    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAll(){
        return (List<Person>) personRepository.findAll();
    }

    public void deleteById(long id) {
        personRepository.deleteById(id);
    }

    public Person createPerson(Person person){
        return personRepository.save(person);
    }

    public Person updatePerson(Person person){
        return personRepository.save(person);
    }

}
