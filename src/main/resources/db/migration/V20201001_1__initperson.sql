create table person
(
	id serial not null,
	firstname varchar default 255,
	lastname varchar default 255,
	city varchar default 255,
	phone bigint,
	telegram varchar default 255
);

create unique index person_id_uindex
	on person (id);

alter table person
	add constraint person_pk
		primary key (id);